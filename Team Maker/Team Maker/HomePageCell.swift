//
//  HomePageCell.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/13/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit

class HomePageCell: UITableViewCell {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.contentView.addDropShadowToView()
    }
    
    func initializeView(data: HomePageCells) {
        logoImageView.image = data.image
        titleLabel.text = data.title
        subtitleLabel.text = data.subtitle
    }
}
