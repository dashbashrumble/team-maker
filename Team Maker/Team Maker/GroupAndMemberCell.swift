//
//  GroupAndMemberCell.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/17/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import Cosmos
import M13Checkbox

protocol MemberCellDelegate: class {
    func selectMember(isSelected: Bool, memberIds: [String], groupId: String?)
}

class GroupAndMemberCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var checkBox: M13Checkbox!
    @IBOutlet weak var ratingsView: CosmosView!
    @IBOutlet weak var ratingsValueLabel: UILabel!
    @IBOutlet weak var groupIndicatorImageView: UIImageView!
    @IBOutlet weak var ratingsViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var checkBoxWidthConstraint: NSLayoutConstraint!
    weak var delegate: MemberCellDelegate?
    private var memberId: String?
    private var groupId: String?
    var selectionType = SelectionType.member
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ratingsView.settings.fillMode = .precise
        ratingsView.settings.minTouchRating = 0.0
    }
    
    func initializeView(data: Selection, disableRating: Bool = false, delegate: MemberCellDelegate?, isSelected: Bool, displayOnly: Bool = false) {
        self.delegate = delegate
        nameLabel.text = data.name
        
        if isSelected == true {
            checkBox.setCheckState(M13Checkbox.CheckState.checked, animated: false)
        } else {
            checkBox.setCheckState(M13Checkbox.CheckState.unchecked, animated: false)
        }
        
        switch data.type {
        case .group:
            groupIndicatorImageView.isHidden = false
            groupIndicatorImageView.image = UIImage(named: "group_ind")
            ratingsView.isHidden = true
            ratingsView.isUserInteractionEnabled = false
            memberId = nil
            groupId = data.id
            self.selectionType = .group
            ratingsValueLabel.text = nil
            ratingsViewWidthConstraint.constant = 30
        case .member:
            ratingsViewWidthConstraint.constant = 120
            self.selectionType = .member
            memberId = data.id
            groupId = nil
            groupIndicatorImageView.image = nil
            groupIndicatorImageView.isHidden = true
            
            if disableRating == true {
                ratingsView.isHidden = false
                ratingsView.isUserInteractionEnabled = false
            } else {
                ratingsView.isHidden = false
                ratingsView.isUserInteractionEnabled = true
            }
            
            var ratings = AppProvider.userDefaults.object(forKey: Keys.memberRatings) as? [String : Double] ?? [String : Double]()
            let rating = ratings[data.id] ?? 0.0
            ratingsView.rating = rating
            ratingsValueLabel.text = String(describing: rating
                .rounded(toPlaces: 1))
            ratingsView.didTouchCosmos = { rating in
                self.ratingsValueLabel.text = String(describing: rating.rounded(toPlaces: 1))
            }
            ratingsView.didFinishTouchingCosmos = { rating in
                self.ratingsValueLabel.text = String(describing: rating.rounded(toPlaces: 1))
                var ratingsCopy = AppProvider.userDefaults.object(forKey: Keys.memberRatings) as? [String : Double] ?? [String : Double]()
                ratingsCopy[data.id] = rating.rounded(toPlaces: 1)
                AppProvider.userDefaults.set(ratingsCopy, forKey: Keys.memberRatings)
            }
            
            if displayOnly == true {
                checkBoxWidthConstraint.constant = 0
                checkBox.isHidden = true
            } else {
                checkBoxWidthConstraint.constant = 30
                checkBox.isHidden = false
            }
        }
    }
    
    @IBAction func valueChanged(_ sender: Any) {
        var isSelected = false
        if checkBox.checkState == M13Checkbox.CheckState.checked {
            isSelected = true
        } else {
            isSelected = false
        }
        switch self.selectionType {
        case .group:
            let groupMembers = AppProvider.userDefaults.object(forKey: Keys.groupMembers) as? [String : [String]] ?? [String : [String]]()
            delegate?.selectMember(isSelected: isSelected, memberIds: groupMembers[groupId ?? ""] ?? [String](), groupId: groupId)
//            self.alpha = 0.5
//            self.isUserInteractionEnabled = false
        case .member:
            if let id = memberId {
                delegate?.selectMember(isSelected: isSelected, memberIds: [id], groupId: groupId)
            }
//            self.alpha = 1.0
//            self.isUserInteractionEnabled = true
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        ratingsView.rating = 0.0
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
