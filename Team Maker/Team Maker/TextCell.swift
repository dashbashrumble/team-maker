//
//  TextCell.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/21/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit

class TextCell: UITableViewCell {

    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var numberOfMembersLabel: UILabel!
    private var id = ""
    
    func initializeView(id: String, name: String, number: Int) {
        self.id = id
        groupNameLabel.text = name
        numberOfMembersLabel.text = String(number) + " Member(s)"
    }
}
