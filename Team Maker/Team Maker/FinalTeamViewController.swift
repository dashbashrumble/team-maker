//
//  FinalTeamViewController.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/25/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import GoogleMobileAds

class FinalTeamViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var selectedPlayerIds = Set<String>()
    var takeRatingsIntoConsideration = false
    var numberOfTeams = 0
    var teamSheet = [String : [String]]()
    var playerRatings = [String : Double]()
    var playerNames = [String : String]()
    var averageRatings = [Double]()
    @IBOutlet weak var notifView: UIView!
    @IBOutlet weak var bannerView: GADBannerView!
    var dateString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playerRatings = AppProvider.userDefaults.object(forKey: Keys.memberRatings) as? [String : Double] ?? [String : Double]()
        playerNames = AppProvider.userDefaults.object(forKey: Keys.members) as? [String : String] ?? [String : String]()
        
        tableView.register(UINib(nibName: "PlayerInfoCell", bundle: Bundle.main), forCellReuseIdentifier: "PlayerInfoCell")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        
        if let teamSheets = AppProvider.userDefaults.object(forKey: Keys.teamSheets) as? [String : [String : [String]]], let date = dateString, let sheet = teamSheets[date] {
            self.notifView.alpha = 0.0
            teamSheet = sheet
        } else {
            var teams = [[String]]()
            for _ in 0...(numberOfTeams - 1) {
                teams.append([String]())
            }
            
            if takeRatingsIntoConsideration == true {
                
                averageRatings = [Double](repeating: 0.0, count: numberOfTeams)
                
                let unsortedArray = Array(selectedPlayerIds)
                let unsortedRatingsArray = unsortedArray.map { playerRatings[$0] ?? 0.0 }
                var sortedRatingsArray = Array(unsortedRatingsArray.sorted().reversed())
                
                var index = 0
                var playerToBeInsertedIndex = 0
                while playerToBeInsertedIndex < selectedPlayerIds.count {
                    
                    if index > numberOfTeams - 1 {
                        index = 0
                    }
                    
                    let nextRating = sortedRatingsArray[playerToBeInsertedIndex]
                    let playerId = getPlayerIdForRating(rating: nextRating)
                    teams[index].append(playerId)
                    
                    index += 1
                    playerToBeInsertedIndex += 1
                }
                
                for j in 0...(teams.count - 1) {
                    let playerIdList = teams[j]
                    let tempRatingsArray = playerIdList.map { playerRatings[$0] ?? 0.0 }
                    var total = 0.0
                    for r in tempRatingsArray {
                        total += r
                    }
                    averageRatings[j] = total / (Double(tempRatingsArray.count))
                }
            } else {
                var playerIds = selectedPlayerIds
                var index = 0
                while playerIds.count > 0 {
                    if index > numberOfTeams - 1 {
                        index = 0
                    }
                    let tempArray = Array(playerIds)
                    let randomIndex = Int(arc4random_uniform(UInt32(tempArray.count)))
                    let insertPlayer = tempArray[randomIndex]
                    teams[index].append(insertPlayer)
                    playerIds.remove(insertPlayer)
                    index += 1
                }
            }
            
            for i in 0...(numberOfTeams - 1) {
                teamSheet["Team \(i + 1)"] = teams[i]
            }
            
            var tSheets = AppProvider.userDefaults.object(forKey: Keys.teamSheets) as? [String : [String : [String]]] ?? [String : [String : [String]]]()
            
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "MMMM dd, yyyy 'at' h:mm a"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
            let dateString = formatter.string(from: Date())
            tSheets[dateString] = teamSheet
            AppProvider.userDefaults.set(tSheets, forKey: Keys.teamSheets)
        }
        
        tableView.reloadData()
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/2375778954"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.5, delay: 4.0, options: .curveEaseOut, animations: {
            self.notifView.alpha = 0.0
        }, completion: nil)
    }

    private func getPlayerIdForRating(rating: Double) -> String {
        for (id, playerRating) in playerRatings {
            if playerRating == rating {
                return id
            }
        }
        return ""
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FinalTeamViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: GlobalConstants.screenWidth - 32, height: 20))
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: FinalTeamSheetHeaderView = FinalTeamSheetHeaderView.fromNib()
        let metaDataText = takeRatingsIntoConsideration ? "Average Rating: \(averageRatings[section].rounded(toPlaces: 1))" : "\(teamSheet["Team \(section + 1)"]?.count ?? 0) Player(s)"
        headerView.initializeView(teamName: "Team \(section + 1)", metaData: metaDataText)
        return headerView
    }
}

extension FinalTeamViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return teamSheet.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teamSheet["Team \(section + 1)"]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerInfoCell") as! PlayerInfoCell
        if let players = teamSheet["Team \(indexPath.section + 1)"] {
            let playerId = players[indexPath.row]
            cell.initializeView(name: playerNames[playerId] ?? "", rating: playerRatings[playerId] ?? 0.0)
        }
        return cell
    }
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

extension FinalTeamViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}
