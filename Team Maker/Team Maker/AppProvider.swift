//
//  AppProvider.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/13/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import Foundation

protocol DataStore {
    associatedtype Store
    var store: Store { get }
}

protocol ReadDataStore {
    func value(forKey: String) -> Any?
    func bool(forKey: String) -> Bool
    func string(forKey: String) -> String?
    func integer(forKey: String) -> Int?
    func double(forKey: String) -> Double?
    func object(forKey: String) -> Any?
}

protocol WriteDataStore {
    func set(_ value: Any?, forKey: String)
    func remove(forKey: String)
    func save()
}

protocol ReadWriteDataStore: ReadDataStore, WriteDataStore {
}

struct SwUserDefaults: DataStore, ReadWriteDataStore {
    typealias Store = UserDefaults
    let store: Store
    
    init() {
        store = UserDefaults.standard
    }
    
    func value(forKey: String) -> Any? {
        return store.value(forKey: forKey)
    }
    
    func bool(forKey: String) -> Bool {
        return store.bool(forKey: forKey)
    }
    
    func string(forKey: String) -> String? {
        return store.string(forKey: forKey)
    }
    
    func integer(forKey: String) -> Int? {
        return store.integer(forKey: forKey)
    }
    
    func double(forKey: String) -> Double? {
        return store.double(forKey: forKey)
    }
    
    func object(forKey: String) -> Any? {
        return store.object(forKey: forKey)
    }
    
    func set(_ value: Any?, forKey: String) {
        return store.set(value, forKey: forKey)
    }
    
    func remove(forKey: String) {
        store.removeObject(forKey: forKey)
    }
    
    func integerValue(forKey: String) -> Int {
        return store.integer(forKey: forKey)
    }
    
    func save() {
        store.synchronize()
    }
}

struct AppProvider {
    static let userDefaults = SwUserDefaults()
}
