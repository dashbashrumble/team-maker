//
//  FinalTeamSheetHeaderView.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/26/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit

class FinalTeamSheetHeaderView: UIView {

    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var metaDataLabel: UILabel!
    
    func initializeView(teamName: String, metaData: String) {
        teamNameLabel.text = teamName
        metaDataLabel.text = metaData
    }
}
