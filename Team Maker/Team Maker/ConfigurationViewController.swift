//
//  ConfigurationViewController.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/25/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import DropDown
import GoogleMobileAds
import Crashlytics

class ConfigurationViewController: UIViewController {

    @IBOutlet weak var teamsLabel: UILabel!
    @IBOutlet weak var totalPlayerCountLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    let dropDown = DropDown()
    var selectedPlayerIds = Set<String>()
    var takeRatingsIntoConsideration = false
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    
    var playerIds = [String]()
    var playerRatings = [String : Double]()
    var players = [String : String]()
    
    func showError(message: String) {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.errorLabel.text = message
            self.errorLabel.alpha = 1.0
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                    self.errorLabel.alpha = 0.0
                }, completion: nil)
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playerIds = Array(selectedPlayerIds)
        playerRatings = AppProvider.userDefaults.object(forKey: Keys.memberRatings) as? [String : Double] ?? [String : Double]()
        players = AppProvider.userDefaults.object(forKey: Keys.members) as? [String : String] ?? [String : String]()
        
        tableView.register(UINib(nibName: "PlayerInfoCell", bundle: Bundle.main), forCellReuseIdentifier: "PlayerInfoCell")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        
        teamsLabel.text = "2"
        totalPlayerCountLabel.text = "Total Player Count: \(selectedPlayerIds.count)"
        dropDown.anchorView = teamsLabel
        let intArray = Array(2...100)
        var strArray = [String]()
        for element in intArray {
            strArray.append("\(element)")
        }
        dropDown.dataSource = strArray
        dropDown.direction = .bottom
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.teamsLabel.text = item
        }
        
        UILabel.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).numberOfLines = 0
        
        DropDown.startListeningToKeyboard()
        let font = UIFont(name: "Quicksand-Medium", size: 12)
        segmentedControl.setTitleTextAttributes([NSAttributedStringKey.font: font ?? UIFont.systemFont(ofSize: 12)],
                                                for: .normal)
        self.errorLabel.text = nil
        self.errorLabel.alpha = 0.0
        
        textField.delegate = self
        textFieldView.layer.borderColor = UIColor.black.cgColor
        textFieldView.layer.borderWidth = 1.0
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MembersListPopoverViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/3832330854"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func showDropdown(_ sender: Any) {
        dropDown.show()
    }
    
    @IBAction func valueChanged(_ sender: Any) {
        if segmentedControl.selectedSegmentIndex == 0 {
            takeRatingsIntoConsideration = false
        } else {
            takeRatingsIntoConsideration = true
        }
    }
    
    @IBAction func proceed(_ sender: Any) {
        if let numberOfTeams = Int(teamsLabel.text!) {
            if numberOfTeams <= selectedPlayerIds.count {
                if let groupName = textField.text, groupName.isEmpty == false {
                    let groupId = randomString(length: 8)
                    var groups = AppProvider.userDefaults.object(forKey: Keys.groups) as? [String : String] ?? [String : String]()
                    groups[groupId] = groupName
                    AppProvider.userDefaults.set(groups, forKey: Keys.groups)
                    var groupMembers = AppProvider.userDefaults.object(forKey: Keys.groupMembers) as? [String : [String]] ?? [String : [String]]()
                    groupMembers[groupId] = Array(selectedPlayerIds)
                    AppProvider.userDefaults.set(groupMembers, forKey: Keys.groupMembers)
                    textField.text = nil
                    view.endEditing(true)
                }
                
                
                Answers.logCustomEvent(withName: "Configuration",
                                       customAttributes: [
                                        "Number of teams": "\(numberOfTeams)",
                                        "Took Ratings Into Consideration": (takeRatingsIntoConsideration ? "Yes" : "No"),
                                        "Group created": textField.text ?? ""
                    ])
                
                self.performSegue(withIdentifier: "FinalTeamViewController", sender: nil)
            } else {
                showError(message: "Number of teams cannot be greater than the number of players")
            }
        }
    }
    
    private func randomString(length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! FinalTeamViewController
        vc.numberOfTeams = Int(teamsLabel.text!) ?? 0
        vc.takeRatingsIntoConsideration = self.takeRatingsIntoConsideration
        vc.selectedPlayerIds = self.selectedPlayerIds
    }
}

extension ConfigurationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: GlobalConstants.screenWidth - 32, height: 20))
        view.backgroundColor = .clear
        return view
    }
}

extension ConfigurationViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return playerIds.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerInfoCell") as! PlayerInfoCell
        let id = playerIds[indexPath.section]
        cell.initializeView(name: players[id] ?? "", rating: playerRatings[id] ?? 0.0)
        return cell
    }
}

extension ConfigurationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension ConfigurationViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}
