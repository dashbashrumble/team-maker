//
//  EditPlayerCell.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/27/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import Cosmos

class EditPlayerCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var ratingsView: CosmosView!
    @IBOutlet weak var ratingsValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func initializeView(name: String, rating: Double) {
        nameLabel.text = name
        ratingsView.rating = rating
        ratingsValueLabel.text = "\(rating)"
    }
}
