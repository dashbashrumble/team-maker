//
//  UIViewExtensions.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/27/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    /**
     The shorthand three-digit hexadecimal representation of color.
     #RGB defines to the color #RRGGBB.
     
     - parameter hex3: Three-digit hexadecimal value.
     - parameter alpha: 0.0 - 1.0. The default is 1.0.
     */
    public convenience init(hex3: UInt16, alpha: CGFloat = 1) {
        let divisor = CGFloat(15)
        let red = CGFloat((hex3 & 0xF00) >> 8) / divisor
        let green = CGFloat((hex3 & 0x0F0) >> 4) / divisor
        let blue = CGFloat(hex3 & 0x00F) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /**
     The shorthand four-digit hexadecimal representation of color with alpha.
     #RGBA defines to the color #RRGGBBAA.
     
     - parameter hex4: Four-digit hexadecimal value.
     */
    public convenience init(hex4: UInt16) {
        let divisor = CGFloat(15)
        let alpha = CGFloat((hex4 & 0xF000) >> 12) / divisor
        let red = CGFloat((hex4 & 0x0F00) >> 8) / divisor
        let green = CGFloat((hex4 & 0x00F0) >> 4) / divisor
        let blue = CGFloat(hex4 & 0x000F) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /**
     The six-digit hexadecimal representation of color of the form #RRGGBB.
     
     - parameter hex6: Six-digit hexadecimal value.
     */
    public convenience init(hex6: UInt32, alpha: CGFloat = 1) {
        let divisor = CGFloat(255)
        let red = CGFloat((hex6 & 0xFF0000) >> 16) / divisor
        let green = CGFloat((hex6 & 0x00FF00) >> 8) / divisor
        let blue = CGFloat(hex6 & 0x0000FF) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /**
     The six-digit hexadecimal representation of color with alpha of the form #RRGGBBAA.
     
     - parameter hex8: Eight-digit hexadecimal value.
     */
    public convenience init(hex8: UInt32) {
        let divisor = CGFloat(255)
        let alpha = CGFloat((hex8 & 0xFF00_0000) >> 24) / divisor
        let red = CGFloat((hex8 & 0x00FF_0000) >> 16) / divisor
        let green = CGFloat((hex8 & 0x0000_FF00) >> 8) / divisor
        let blue = CGFloat(hex8 & 0x0000_00FF) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /**
     The rgba string representation of color with alpha of the form #RRGGBBAA/#RRGGBB, throws error.
     
     - parameter rgba: String value.
     */
    public convenience init?(hexString: String?) {
        guard var rgba = hexString else { return nil }
        if !rgba.hasPrefix("#") {
            rgba = "#\(rgba)"
        }
        let hexString: String = String(rgba[String.Index.init(encodedOffset: 1)...])
        var hexValue:  UInt32 = 0
        guard Scanner(string: hexString).scanHexInt32(&hexValue) else {
            return nil
        }
        switch (hexString.count) {
        case 3:
            self.init(hex3: UInt16(hexValue))
        case 4:
            self.init(hex4: UInt16(hexValue))
        case 6:
            self.init(hex6: hexValue)
        case 8:
            self.init(hex8: hexValue)
        default:
            return nil
        }
    }
    
    /**
     The rgba string representation of color with alpha of the form #RRGGBBAA/#RRGGBB, fails to default color.
     
     - parameter rgba: String value.
     */
    //    public convenience init(rgba: String, defaultColor: UIColor = UIColor.clearColor()) {
    //        guard let color = try? UIColor(rgba_throws: rgba) else {
    //            self.init(CGColor: defaultColor.CGColor)
    //            return
    //        }
    //        self.init(CGColor: color.CGColor)
    //    }
    
    /**
     Hex string of a UIColor instance.
     
     - parameter rgba: Whether the alpha should be included.
     */
    public func hexString(_ includeAlpha: Bool) -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        if includeAlpha {
            return String(format: "#%02X%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255), Int(a * 255))
        } else {
            return String(format: "#%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255))
        }
    }
}

extension UIColor {
    
    public convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: 1)
    }
    
    public convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) {
        self.init(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: a)
    }
}


extension UIColor {
    
    class var tmBlue: UIColor {
        return UIColor(red: 56.0 / 255.0, green: 140.0 / 255.0, blue: 241.0 / 255.0, alpha: 1.0)
    }
    
    class var tdTextColor: UIColor {
        return UIColor(r: 209, g: 222, b: 237)
    }
    
    class var paYellow: UIColor {
        return UIColor(r: 255, g: 195, b: 15)
    }
    
    class var topViewGradient: UIColor {
        return UIColor(r: 232, g: 237, b: 242)
    }
    
    class var ratingStar: UIColor {
        return UIColor(r: 255, g: 196, b: 0)
    }
    
    class var swOrange110: UIColor {
        return UIColor(red: 226.0 / 255.0, green: 114.0 / 255.0, blue: 23.0 / 255.0, alpha: 1.0)
    }
    
    class var swBlackGrape90: UIColor {
        return UIColor(red: 61.0 / 255.0, green: 65.0 / 255.0, blue: 82.0 / 255.0, alpha: 1.0)
    }
    
    class var swBlackGrape70: UIColor {
        return UIColor(red: 104.0 / 255.0, green: 107.0 / 255.0, blue: 120.0 / 255.0, alpha: 1.0)
    }
    
    class var swBlackGrape60: UIColor {
        return UIColor(red: 126.0 / 255.0, green: 128.0 / 255.0, blue: 140.0 / 255.0, alpha: 1.0)
    }
    
    class var swOrange30: UIColor {
        return UIColor(red: 254.0 / 255.0, green: 204.0 / 255.0, blue: 163.0 / 255.0, alpha: 1.0)
    }
    
    class var swOrange70: UIColor {
        return UIColor(red: 252.0 / 255.0, green: 166.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)
    }
    
    class var swOrange100: UIColor {
        return UIColor(red: 252.0 / 255.0, green: 128.0 / 255.0, blue: 26.0 / 255.0, alpha: 1.0)
    }
    
    class var dot: UIColor {
        return UIColor(r: 151, g: 151, b: 151)
    }
    
    class var swPumpkinOrange: UIColor {
        return UIColor(r: 252, g: 128, b: 26)
    }
    
    class var swBlackGrape80: UIColor {
        return UIColor(red: 83.0 / 255.0, green: 87.0 / 255.0, blue: 102.0 / 255.0, alpha: 1.0)
    }
    
    class var swBlackGrape50: UIColor {
        return UIColor(red: 147.0 / 255.0, green: 149.0 / 255.0, blue: 159.0 / 255.0, alpha: 1.0)
    }
    
    class var swBlackGrape40: UIColor {
        return UIColor(red: 169.0 / 255.0, green: 171.0 / 255.0, blue: 178.0 / 255.0, alpha: 1.0)
    }
    
    class var swBlackGrape30: UIColor {
        return UIColor(red: 190.0 / 255.0, green: 191.0 / 255.0, blue: 197.0 / 255.0, alpha: 1.0)
    }
    
    class var swBlackGrape20: UIColor {
        return UIColor(red: 212.0 / 255.0, green: 213.0 / 255.0, blue: 217.0 / 255.0, alpha: 1.0)
    }
    
    class var swBlackGrape10: UIColor {
        return UIColor(red: 233.0 / 255.0, green: 233.0 / 255.0, blue: 235.0 / 255.0, alpha: 1.0)
    }
    
    class var swBlackGrape5: UIColor {
        return UIColor(red: 244.0 / 255.0, green: 244.0 / 255.0, blue: 245.0 / 255.0, alpha: 1.0)
    }
}
