//
//  EditPlayerViewController.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/27/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import GoogleMobileAds

class EditPlayerViewController: UIViewController {

    @IBOutlet weak var totalPlayersLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addErrorLabel: UILabel!
    @IBOutlet weak var bannerView: GADBannerView!
    var players = [Selection]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        tableView.register(UINib(nibName: "GroupAndMemberCell", bundle: Bundle.main), forCellReuseIdentifier: "GroupAndMemberCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        textFieldView.layer.borderColor = UIColor.black.cgColor
        textFieldView.layer.borderWidth = 1.0
        
        self.addErrorLabel.alpha = 0.0
        self.addErrorLabel.text = nil
        
        textField.delegate = self
        
        let members = AppProvider.userDefaults.object(forKey: Keys.members) as? [String : String] ?? [String : String]()
        for (id, name) in members {
            players.append(Selection(id: id, name: name, type: .member, numberOfMembers: 0))
        }
        totalPlayersLabel.text = "Total Players: \(members.count)"
        
        if players.count == 0 {
            errorLabel.isHidden = false
        } else {
            errorLabel.isHidden = true
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MembersListPopoverViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/8530123769"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func addPlayer(_ sender: Any) {
        guard let text = textField.text else { return }
        if text.isEmpty == false {
            
            var members = AppProvider.userDefaults.object(forKey: Keys.members) as? [String : String] ?? [String : String]()
            let memberId = randomString(length: 8)
            members[memberId] = text
            AppProvider.userDefaults.set(members, forKey: Keys.members)
            
            var memberRatings = AppProvider.userDefaults.object(forKey: Keys.memberRatings) as? [String : Double] ?? [String : Double]()
            memberRatings[memberId] = 0.0
            AppProvider.userDefaults.set(memberRatings, forKey: Keys.memberRatings)
            
            players.insert(Selection(id: memberId, name: text, type: .member, numberOfMembers: 0), at: 0)
            tableView.insertSections(IndexSet(integer: 0), with: UITableViewRowAnimation.automatic)
            errorLabel.isHidden = true
            
            textField.text = nil
            totalPlayersLabel.text = "Total Players: \(members.count)"
        } else {
            showError(message: "Please enter a valid name")
        }
    }
    
    private func randomString(length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    func showError(message: String) {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.addErrorLabel.text = message
            self.addErrorLabel.alpha = 1.0
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                    self.addErrorLabel.alpha = 0.0
                }, completion: nil)
            })
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditPlayerViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: GlobalConstants.screenWidth - 32, height: 20))
        view.backgroundColor = .clear
        return view
    }
}

extension EditPlayerViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension EditPlayerViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return players.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupAndMemberCell") as! GroupAndMemberCell
        let player = players[indexPath.section]
        cell.initializeView(data: player, delegate: nil, isSelected: false, displayOnly: true)
        return cell
    }
}

extension EditPlayerViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}
