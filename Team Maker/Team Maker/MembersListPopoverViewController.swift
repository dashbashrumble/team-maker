//
//  MembersListPopoverViewController.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/19/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import Crashlytics

class MembersListPopoverViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var trashButton: UIButton!
    weak var delegate: GroupDelegate?
    
    var members = [Selection]()
    var selectedMemberIds = Set<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldView.layer.borderColor = UIColor.black.cgColor
        textFieldView.layer.borderWidth = 1.0
        textField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "GroupAndMemberCell", bundle: Bundle.main), forCellReuseIdentifier: "GroupAndMemberCell")
        
        let memberList = AppProvider.userDefaults.object(forKey: Keys.members) as? [String : String] ?? [String : String]()
        for (id, name) in memberList {
            members.append(Selection(id: id, name: name, type: .member, numberOfMembers: 0))
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MembersListPopoverViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        errorLabel.text = "    "
        errorLabel.alpha = 0.0
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func done(_ sender: Any) {
        if textField.text?.isEmpty == true {
            showError(message: "Please enter a group name")
        } else if selectedMemberIds.count < 2 {
            showError(message: "Please select at least 2 players")
        } else {
            let groupId = randomString(length: 8)
            var groups = AppProvider.userDefaults.object(forKey: Keys.groups) as? [String : String] ?? [String : String]()
            groups[groupId] = textField.text!
            AppProvider.userDefaults.set(groups, forKey: Keys.groups)
            var groupMembers = AppProvider.userDefaults.object(forKey: Keys.groupMembers) as? [String : [String]] ?? [String : [String]]()
            groupMembers[groupId] = Array(selectedMemberIds)
            AppProvider.userDefaults.set(groupMembers, forKey: Keys.groupMembers)
            view.endEditing(true)
            delegate?.reloadTableView()
            self.dismiss(animated: true, completion: nil)
            
            Answers.logCustomEvent(withName: "New Group Created",
                                   customAttributes: [
                                    "Name": textField.text ?? ""
                ])
        }
    }
    
    private func randomString(length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    @IBAction func trash(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func showError(message: String) {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.errorLabel.text = message
            self.errorLabel.alpha = 1.0
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                    self.errorLabel.alpha = 0.0
                    self.errorLabel.text = "     "
                }, completion: nil)
            })
        }
    }
}

extension MembersListPopoverViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension MembersListPopoverViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: GlobalConstants.screenWidth - 32, height: 20))
        view.backgroundColor = .clear
        return view
    }
}

extension MembersListPopoverViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return members.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupAndMemberCell") as! GroupAndMemberCell
        let member = members[indexPath.section]
        cell.initializeView(data: member, disableRating: true, delegate: self, isSelected: selectedMemberIds.contains(member.id))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension MembersListPopoverViewController: MemberCellDelegate {
    func selectMember(isSelected: Bool, memberIds: [String], groupId: String?) {
        if isSelected == true {
            for id in memberIds {
                selectedMemberIds.insert(id)
            }
        } else {
            for id in memberIds {
                selectedMemberIds.remove(id)
            }
        }
    }
}
