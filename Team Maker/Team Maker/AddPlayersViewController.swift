//
//  AddPlayersViewController.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/27/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit

class AddPlayersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalPlayersLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var tableViewErrorLabel: UILabel!
    weak var delegate: UpdateGroupDelegate?
    
    var selectedPlayerIds = Set<String>()
    var existingMembers = [Selection]()
    var cards = [Selection]()
    var groupId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "GroupAndMemberCell", bundle: Bundle.main), forCellReuseIdentifier: "GroupAndMemberCell")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        
        let members = AppProvider.userDefaults.object(forKey: Keys.members) as? [String : String] ?? [String : String]()
        for (id, name) in members {
            cards.append(Selection(id: id, name: name, type: .member, numberOfMembers: 0))
        }
        
        errorLabel.alpha = 0.0
        
        var nonMatchingMemberFound = false
        if cards.count == existingMembers.count {
            for x in cards {
                var isFound = false
                for y in existingMembers {
                    if x.id == y.id {
                        isFound = true
                    }
                }
                if isFound == false {
                    nonMatchingMemberFound = true
                    break
                }
            }
        } else {
            nonMatchingMemberFound = true
        }

        for x in 0...(existingMembers.count - 1) {
            for y in 0...(cards.count - 1) {
                if cards[y].id == existingMembers[x].id {
                    cards.remove(at: y)
                    break
                }
            }
        }
        
        if nonMatchingMemberFound == true {
            tableViewErrorLabel.isHidden = true
            totalPlayersLabel.text = "Total Players Selected: \(selectedPlayerIds.count)"
        } else {
            tableViewErrorLabel.isHidden = false
            totalPlayersLabel.text = nil
            cards.removeAll()
        }
    }
    
    func showError() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.errorLabel.alpha = 1.0
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                    self.errorLabel.alpha = 0.0
                }, completion: nil)
            })
        }
    }

    @IBAction func done(_ sender: Any) {
        if selectedPlayerIds.count <= 0 {
            showError()
        } else {
            var groupMembers = AppProvider.userDefaults.object(forKey: Keys.groupMembers) as? [String : [String]] ?? [String : [String]]()
            if var group = groupMembers[groupId] {
                let newPlayers = Array(selectedPlayerIds)
                for p in newPlayers {
                    group.append(p)
                }
                groupMembers[groupId] = group
                AppProvider.userDefaults.set(groupMembers, forKey: Keys.groupMembers)
            }
            delegate?.reloadTableView()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func closeView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddPlayersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: GlobalConstants.screenWidth - 32, height: 20))
        view.backgroundColor = .clear
        return view
    }
}

extension AddPlayersViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupAndMemberCell") as! GroupAndMemberCell
        let card = cards[indexPath.section]
        cell.initializeView(data: card, disableRating: true, delegate: self, isSelected: selectedPlayerIds.contains(card.id))
        return cell
    }
}

extension AddPlayersViewController: MemberCellDelegate {
    func selectMember(isSelected: Bool, memberIds: [String], groupId: String?) {
        if isSelected == true {
            for id in memberIds {
                selectedPlayerIds.insert(id)
            }
        } else {
            for id in memberIds {
                selectedPlayerIds.remove(id)
            }
        }
        if groupId != nil {
            tableView.reloadData()
        }
        totalPlayersLabel.text = "Total Players Selected: \(selectedPlayerIds.count)"
    }
}

