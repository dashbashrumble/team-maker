//
//  ManageGroupsViewController.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/19/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import MMTransition
import GoogleMobileAds

protocol GroupDelegate: class {
    func reloadTableView()
}

class ManageGroupsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var createNewGroupButton: UIButton!
    @IBOutlet weak var emptyTableViewErrorLabel: UILabel!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var groups = [Selection]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.register(UINib(nibName: "TextCell", bundle: Bundle.main), forCellReuseIdentifier: "TextCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/2499067883"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0
    }

    override func viewWillAppear(_ animated: Bool) {
        reloadTableView()
        if groups.count <= 0 {
            emptyTableViewErrorLabel.isHidden = false
        }
    }
    
    @IBAction func createNewGroup(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let second = story.instantiateViewController(withIdentifier: "MembersListPopoverViewController") as! MembersListPopoverViewController
        second.delegate = self
        second.mmT.present.menu { (config) in
            config.isDraggable = true
            config.presentingScale = 1.0
            config.menuType = .bottomHeight(h: GlobalConstants.screenHeight * 0.75)
        }
        self.present(second, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let group = sender as! Selection
        let vc = segue.destination as! UpdateGroupViewController
        vc.groupId = group.id
        vc.groupName = group.name
    }
}

extension ManageGroupsViewController: GroupDelegate {
    func reloadTableView() {
        groups.removeAll()
        
        let groupMembers = AppProvider.userDefaults.object(forKey: Keys.groupMembers) as? [String : [String]] ?? [String : [String]]()
        
        let groupList = AppProvider.userDefaults.object(forKey: Keys.groups) as? [String : String] ?? [String : String]()
        for (id, name) in groupList {
            groups.append(Selection(id: id, name: name, type: .group, numberOfMembers: (groupMembers[id]?.count ?? 0)))
        }
        tableView.reloadData()
        emptyTableViewErrorLabel.isHidden = true
    }
}

extension ManageGroupsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: GlobalConstants.screenWidth - 32, height: 20))
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let group = groups[indexPath.section]
        self.performSegue(withIdentifier: "UpdateGroupViewController", sender: group)
    }
}

extension ManageGroupsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return groups.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextCell") as! TextCell
        let card = groups[indexPath.section]
        cell.initializeView(id: card.id, name: card.name, number: card.numberOfMembers)
        return cell
    }
}

extension ManageGroupsViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}
