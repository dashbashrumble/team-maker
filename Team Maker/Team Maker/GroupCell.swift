//
//  GroupCell.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/19/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit

class GroupCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    private var id = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func initializeView(id: String, name: String) {
        self.id = id
        nameLabel.text = name
    }
}
