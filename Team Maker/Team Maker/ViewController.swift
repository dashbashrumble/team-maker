//
//  ViewController.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/13/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Crashlytics

enum HomePageCells {
    case createTeam
    case groups
    case teamSheets
    case memberList
    
    var title: String {
        switch self {
        case .createTeam:
            return "Make Teams"
        case .groups:
            return "My Groups"
        case .teamSheets:
            return "My Team Sheets"
        case .memberList:
            return "Player List"
        }
    }
    
    var subtitle: String {
        switch self {
        case .createTeam:
            return "You can start creating your teams here"
        case .groups:
            return "View and edit any groups that you may have saved"
        case .teamSheets:
            return "View a dated list of all teams ever created"
        case .memberList:
            return "You may add new members or edit existing members"
        }
    }
    
    var image: UIImage {
        switch self {
        case .createTeam:
            return UIImage(named: "team")!
        case .groups:
            return UIImage(named: "groups")!
        case .teamSheets:
            return UIImage(named: "teamSheets")!
        case .memberList:
            return UIImage(named: "memberList")!
        }
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    let cards: [HomePageCells] = [.createTeam, .groups, .teamSheets, .memberList]
    var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }
    var localTimeZoneName: String { return TimeZone.current.identifier }
    var systemVersion: String { return UIDevice.current.systemVersion }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "HomePageCell", bundle: Bundle.main), forCellReuseIdentifier: "HomePageCell")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/6651470850"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0
        
        if AppProvider.userDefaults.object(forKey: Keys.launchEvents) == nil {
            AppProvider.userDefaults.set("Events sent", forKey: Keys.launchEvents)
            Answers.logCustomEvent(withName: "Launch",
                                   customAttributes: [
                                    "Time": getTodayString(),
                                    "Time Zone Abbreviation": localTimeZoneAbbreviation,
                                    "Time Zone Name": localTimeZoneName,
                                    "OS Version": systemVersion
                ])
            if let countryCode = NSLocale.current.regionCode {
                Answers.logCustomEvent(withName: "Country",
                                       customAttributes: [
                                        "Country Code": countryCode
                    ])
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.3, delay: 0.5, options: .curveEaseOut, animations: {
            self.imageView.alpha = 0.0
        }, completion: nil)
    }
    
    private func getTodayString() -> String{
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        
        let year = components.year
        let month = components.month
        let day = components.day
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        let today_string = String(year!) + "-" + String(month!) + "-" + String(day!) + " " + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        
        return today_string
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: GlobalConstants.screenWidth - 32, height: 20))
        view.backgroundColor = .clear
        return view
    }
}

extension ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomePageCell") as! HomePageCell
        let card = cards[indexPath.section]
        cell.initializeView(data: card)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let card = cards[indexPath.section]
        switch card {
        case .createTeam:
            self.performSegue(withIdentifier: "MakeTeams", sender: nil)
        case .groups:
            self.performSegue(withIdentifier: "ShowGroups", sender: nil)
        case .teamSheets:
            self.performSegue(withIdentifier: "ViewTeamSheets", sender: nil)
        case .memberList:
            self.performSegue(withIdentifier: "EditPlayerViewController", sender: nil)
        }
        
        Answers.logCustomEvent(withName: "Selected Section",
                               customAttributes: [
                                "Category": card.title
            ])
    }
}

extension UIView {
    func addDropShadowToView(){
        self.layer.masksToBounds =  false
        self.layer.shadowColor = UIColor.darkGray.cgColor;
        self.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        self.layer.shadowOpacity = 1.0
    }
}

extension ViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}
