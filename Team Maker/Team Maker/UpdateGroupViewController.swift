//
//  UpdateGroupViewController.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/27/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import GoogleMobileAds

protocol UpdateGroupDelegate: class {
    func reloadTableView()
}

extension UpdateGroupViewController: UpdateGroupDelegate {
    func reloadTableView() {
        players.removeAll()
        let groupMembers = AppProvider.userDefaults.object(forKey: Keys.groupMembers) as? [String : [String]] ?? [String : [String]]()
        let members = AppProvider.userDefaults.object(forKey: Keys.members) as? [String : String] ?? [String : String]()
        if let playerList = groupMembers[groupId] {
            for p in playerList {
                players.append(Selection(id: p, name: members[p] ?? "", type: .member, numberOfMembers: 0))
            }
        }
        tableView.reloadData()
    }
}

class UpdateGroupViewController: UIViewController {

    @IBOutlet weak var addNewPlayersButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bannerView: GADBannerView!
    var ratings = [String : Double]()
    
    var groupName = ""
    var groupId = ""
    var players = [Selection]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = groupName
        tableView.register(UINib(nibName: "EditPlayerCell", bundle: Bundle.main), forCellReuseIdentifier: "EditPlayerCell")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        errorLabel.alpha = 0.0
        
        ratings = AppProvider.userDefaults.object(forKey: Keys.memberRatings) as? [String : Double] ?? [String : Double]()
        var groupMembers = AppProvider.userDefaults.object(forKey: Keys.groupMembers) as? [String : [String]] ?? [String : [String]]()
        var members = AppProvider.userDefaults.object(forKey: Keys.members) as? [String : String] ?? [String : String]()
        
        if let playerList = groupMembers[groupId] {
            for p in playerList {
                players.append(Selection(id: p, name: members[p] ?? "", type: .member, numberOfMembers: 0))
            }
        }
        
        addNewPlayersButton.layer.borderWidth = 1.0
        addNewPlayersButton.layer.borderColor = UIColor.tmBlue.cgColor
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/4669935654"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0
    }
    
    func showError() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.errorLabel.alpha = 1.0
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                    self.errorLabel.alpha = 0.0
                }, completion: nil)
            })
        }
    }
    
    @IBAction func addNewPlayers(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let second = story.instantiateViewController(withIdentifier: "AddPlayersViewController") as! AddPlayersViewController
        second.existingMembers = self.players
        second.delegate = self
        second.groupId = self.groupId
        second.mmT.present.menu { (config) in
            config.isDraggable = true
            config.presentingScale = 1.0
            config.menuType = .bottomHeight(h: GlobalConstants.screenHeight * 0.75)
        }
        self.present(second, animated: true, completion: nil)
    }
    
    @IBAction func update(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension UpdateGroupViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: GlobalConstants.screenWidth - 32, height: 20))
        view.backgroundColor = .clear
        return view
    }
}

extension UpdateGroupViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return players.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditPlayerCell") as! EditPlayerCell
        let player = players[indexPath.section]
        cell.initializeView(name: player.name, rating: ratings[player.id] ?? 0.0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let playerId = players[indexPath.section].id
        var groupMembers = AppProvider.userDefaults.object(forKey: Keys.groupMembers) as? [String : [String]] ?? [String : [String]]()
        if var playerList = groupMembers[groupId] {
            if playerList.count <= 2 {
                showError()
            } else {
                for i in 0...(playerList.count - 1) {
                    if playerList[i] == playerId {
                        playerList.remove(at: i)
                        players.remove(at: indexPath.section)
                        tableView.deleteSections(IndexSet(integer: indexPath.section), with: UITableViewRowAnimation.automatic)
                        break
                    }
                }
                groupMembers[groupId] = playerList
                AppProvider.userDefaults.set(groupMembers, forKey: Keys.groupMembers)
            }
        }
    }
}

extension UpdateGroupViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}
