//
//  TeamSheetCell.swift
//  Team Maker
//
//  Created by Rahul Manghnani on 5/26/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit

class TeamSheetCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var teamsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func initializeView(date: String, teams: String) {
        dateLabel.text = date
        teamsLabel.text = teams
    }
}
